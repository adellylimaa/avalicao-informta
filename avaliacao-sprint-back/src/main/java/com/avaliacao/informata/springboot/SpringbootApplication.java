package com.avaliacao.informata.springboot;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class SpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootApplication.class, args);
	}

	@RestController
	@RequestMapping("avaliacao")
	public class AvaliacaoRestController {

		@CrossOrigin
		@GetMapping("clientes")
		public List<Map<String, Object>> clientesAvaliacaoMock(){
			List<Map<String, Object>> lista = new ArrayList<>();
			Map<String, Object> mapClientes = new HashMap<>();
			mapClientes.put("id", 1);
			mapClientes.put("nome", "José");
			mapClientes.put("cpf", "088.755.823-98");
			lista.add(mapClientes);

			return lista;						
		}
	}


}

